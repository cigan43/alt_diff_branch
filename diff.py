import json
import requests
from dataclasses import dataclass
from distutils.version import StrictVersion

# @dataclass
# class Package:
#     name: str
#     epoch: int
#     version: str
#     release: str
#     arch: str
#     disttag: str
#     buildtime: int
#     source: str

@dataclass
class Out_date:
    arch: str
    name: str
    version: str


class GetJson():
    def __init__(self, url):
        self.url = url

    def GetBranch(self):
        try:
            branch = requests.get(self.url, headers={'Accept': 'application/json'})
            branch.raise_for_status()
            return branch.json()
        except requests.exceptions.RequestException as err:
            print(self.url, err)


class ParserJson():
    def __init__(self, json_master, json_slave):
        self.json_master = json_master
        self.json_slave = json_slave

    def Check_dict(self, branch_dict):
        if branch_dict.get('errors'):
            return True

    def Get_Package(self, branch_json: json):
        dict_packages = {}
        for json_package in branch_json["packages"]:
            if dict_packages.get(json_package["arch"]):
                dict_packages[json_package["arch"]][json_package["name"]] = json_package["version"]
            else:
                dict_packages[json_package["arch"]] = {}
                dict_packages[json_package["arch"]][json_package["name"]] = json_package["version"]
        return dict_packages



    def Search_Item(self, name, slave_package_dict):
        slave_pack = slave_package_dict.get(name)
        print(slave_pack)
        if slave_pack is None:
            return dict(name=None)
        else:
            return {name:slave_pack}


    def Diff_Dict(self, check_version=0):
        out_list = []
        master_packages = self.Get_Package(self.json_master)
        slave_packages = self.Get_Package(self.json_slave)
        print(master_packages["aarch64"].get("0ad"), "++++++")
        print(slave_packages["aarch64"].get("0ad"), "-----------")
        for arch in master_packages.keys():
            for name in master_packages[arch].keys():
                diff_master_slave = self.Search_Item(name, slave_packages[arch])
                print(name, diff_master_slave, master_packages[arch][name])
                if diff_master_slave.get(name) is None:
                    out_list.appent(Out_date(arch=arch, name=name, version=None))
                elif check_version == 0:
                    if StrictVersion(master_packages[arch][name]) > StrictVersion(diff_master_slave[name]):
                        out_list.appent(Out_date(arch=arch, name=name, version=master_packages[arch][name]))
        return out_list
