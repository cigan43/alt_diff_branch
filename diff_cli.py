import argparse
import diff
import json

BASE_URL = "https://rdb.altlinux.org/api/export/branch_binary_packages/"

def read_argument():
    parser = argparse.ArgumentParser()
    parser.add_argument("-b1", "--branch_master", help="master branch, with which they compare, default p10", default="p10")
    parser.add_argument("-b2", "--branch_slave", help="slave branch, default p9", default="p9")
    parser.add_argument("-a", "--arch", help="arch, default all", default="all")
    parser.add_argument("-o", "--out", help="out in file, default STDOUT", default="STDOUT")
    return parser.parse_args()


def write_file(filename, data):
    with open(filename, 'w') as outfile:
        json.dump(data, outfile)
    return True


def main():
    arguments = read_argument()

    if arguments.arch != "all":
        branch_master = diff.GetJson(f"{BASE_URL}{arguments.branch_master}?arch={arguments.arch}").GetBranch()
        branch_slave = diff.GetJson(f"{BASE_URL}{arguments.branch_slave}?arch={arguments.arch}").GetBranch()
    else:
        branch_master = diff.GetJson(f"{BASE_URL}{arguments.branch_master}").GetBranch()
        branch_slave = diff.GetJson(f"{BASE_URL}{arguments.branch_slave}").GetBranch()


    # if not branch_master  or not branch_slave:
    #     print("not connect with rdb.altlinux.org")
    #     return

    parserjson = diff.ParserJson(branch_master, branch_slave)
    diff_master_dict = parserjson.Diff_Dict()
    for i in diff_master_dict:
        print(i)
    # parserjson = diff.ParserJson(branch_slave.GetBranch(), branch_master.GetBranch())
    # diff_slave_dict = parserjson.DiffDict(check_version=1)
    # if not diff_master_dict:
    #     print("you entered the architecture incorrectly")

    # final_dictionary = {arguments.branch_master: diff_master_dict, arguments.branch_slave: diff_slave_dict}

    # if arguments.out == "STDOUT":
    #     print(json.dumps(final_dictionary))
    # else:
    #     write_file(arguments.out, final_dictionary)


if __name__ == "__main__":
    try:
        main()
    except SystemExit:
        SystemExit(0)
